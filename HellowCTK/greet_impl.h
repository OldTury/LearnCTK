// hello_impl.h
#ifndef HELLO_IMPL_H
#define HELLO_IMPL_H

#include "hello_service.h"
#include <QObject>

class ctkPluginContext;

class GreetImpl : public QObject, public HelloService, public ByeService
{
    Q_OBJECT
    Q_INTERFACES(HelloService)
    Q_INTERFACES(ByeService)

public:
    GreetImpl();
    void sayHello() Q_DECL_OVERRIDE;
    void sayBye() Q_DECL_OVERRIDE;
};

#endif // HELLO_IMPL_H
