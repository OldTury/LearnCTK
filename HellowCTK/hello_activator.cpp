// hello_activator.cpp
#include "hello_activator.h"
#include "hello_impl.h"
#include "greet_impl.h"

void HelloActivator::start(ctkPluginContext* context)
{
    m_pImpl = new GreetImpl();

    // 注册服务
    context->registerService<HelloService>(m_pImpl);
    context->registerService<ByeService>(m_pImpl);
}

void HelloActivator::stop(ctkPluginContext* context)
{
    Q_UNUSED(context)
}
