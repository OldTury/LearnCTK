// hello_impl.cpp
#include "greet_impl.h"
#include <ctkPluginContext.h>
#include <QtDebug>

GreetImpl::GreetImpl(ctkPluginContext* context)
{
    context->registerService<HelloService>(this);
}

void GreetImpl::sayHello()
{
    qDebug() << "Hello,CTK!";
}

void GreetImpl::sayBye()
{
    qDebug() << "Bye, CTK!";
}
